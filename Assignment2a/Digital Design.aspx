﻿<%@ Page Title="Digital Design" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Digital Design.aspx.cs" Inherits="Assignment2a.Digital_Design" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TrickyConcept" runat="server">
    <h3>Tricky Concept in Digital Design - Using table tag</h3>
    <p>The table tag is used in HTML to create a Table in the webpage.
        The HTML tables allow web authors to arrange data like text, images, links, other tables, etc. into rows and columns of cells.
    </p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CodeSnippet" runat="server">
    <h3>Code snippet to create a table having two columns (First Name,Last Name) </h3>
        <table border ="1">
            <thead>
                <tr>
                    <th> First Name </th>
                    <th> Last Name </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> John </td>
                    <td> Doe </td>              
                </tr>
            </tbody>
        </table>        <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ExampleSnippetCode" runat="server">
    <h3>This creates a table showing the cell id's as the table data</h3>
        <table border = "1">
         <tr>
            <td>Row 1, Column 1</td>
            <td>Row 1, Column 2</td>
         </tr>
         
         <tr>
            <td>Row 2, Column 1</td>
            <td>Row 2, Column 2</td>
         </tr>
      </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="HelpFullLinks" runat="server">
    <h3>Useful Resource links</h3>
    <a href="https://www.w3schools.com/html/html_tables.asp">W3 Schools</a><br />
    <a href="https://www.tutorialspoint.com/html/html_tables.htm">Tutorials Point</a><br />
</asp:Content>
